/*
 * Copyright King_DuckZ (2012)
 *
 * This file is part of WatchAndMirror.
 *
 * Nome-Programma is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nome-Programma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id7976387E4D2348A6B13108947F3F3B1B
#define id7976387E4D2348A6B13108947F3F3B1B

#include "Action.hpp"
#include <sys/inotify.h>

namespace wam {
	class WatchedObject {
	public:
		WatchedObject ( const inotify_event* parEvent, const std::map<int, std::string>& parPathDict, Action parAction );
		WatchedObject ( const inotify_event* parEvent, const inotify_event* parEvent2, const std::map<int, std::string>& parPathDict );
		~WatchedObject ( void ) {}
	
		bool operator== ( const WatchedObject& parOther ) const { return m_watchID == parOther.m_watchID and m_filename == parOther.m_filename; }
		bool operator< ( const WatchedObject& parOther ) const;
	
		Action GetAction ( void ) const { return m_pendingAction; }
		const std::string& GetFirstFilename ( void ) const { return m_filename; }
		const std::string& GetSecondFilename ( void ) const { return m_filename2; }
		const std::string& GetPath ( void ) const { return m_path; }
		const std::string& GetSecondPath ( void ) const { return m_path2; }
	
	private:
		const std::string m_path;
		const std::string m_path2;
		const std::string m_filename;
		const std::string m_filename2;
		const int m_watchID;
		const Action m_pendingAction;
	
		WatchedObject ( const WatchedObject& ); //Forbidden
		const WatchedObject& operator= ( const WatchedObject& ); //Forbidden
	};
}

#endif
