/*
 * Copyright King_DuckZ (2012)
 *
 * This file is part of WatchAndMirror.
 *
 * Nome-Programma is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nome-Programma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id7465CF2CAA464A23B43E4F1086BC6446
#define id7465CF2CAA464A23B43E4F1086BC6446

namespace wam {
	enum Action {
		Action_Delete,
		Action_Create,
		Action_Edit,
		Action_Move,
		Action_Attrib
	};
} //namespace wam

#endif
