/*
 * Copyright King_DuckZ (2012)
 *
 * This file is part of WatchAndMirror.
 *
 * Nome-Programma is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nome-Programma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef idD218916FEB7F4FD39097025CCEBEF7A9
#define idD218916FEB7F4FD39097025CCEBEF7A9

#include "Mutex.hpp"
#include "Action.hpp"
#include <stdint.h>

namespace wam {
	class WatchedObject;

	class FileOpCostIsMore : public std::binary_function<WatchedObject, WatchedObject, bool> {
		typedef std::binary_function<WatchedObject, WatchedObject, bool> parent;
	public:
		typedef parent::first_argument_type first_argument_type;
		typedef parent::second_argument_type second_argument_type;
		typedef parent::result_type result_type;
	
		bool operator() ( const WatchedObject* parA, const WatchedObject* parB ) const;
	};

	class ActionManager {
		typedef std::priority_queue<WatchedObject*, std::vector<WatchedObject*>, FileOpCostIsMore> QueueType;
	
	public:
		ActionManager ( void );
		~ActionManager ( void );
	
		void AddMonitoredDirectory ( const char* parPath );
		void WaitIfEmpty ( void );
		bool Update ( char* parBuffer, int parBufferSize );
		bool Pop ( Action& parAction, std::string& parPath, std::string& parPathSecond, std::string& parFile, std::string& parFileSecond );
		void NotifyQuitting ( void ) { m_waitIfEmpty.Set(); }
	
	private:
		struct PackagedData;

		void AddFileMoveAction ( const PackagedData& parData );
		void AddAction ( Action parAction, const PackagedData& parData );

		std::map<int, std::string> m_watchedPaths;
		std::map<uint32_t, char*> m_moves;
		QueueType m_queue;
		BlockingCondition m_waitIfEmpty;
		Mutex m_queueMutex;
		int m_fd;
	};
} //namespace wam

#endif
