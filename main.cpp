/*
 * Copyright King_DuckZ (2012)
 *
 * This file is part of WatchAndMirror.
 *
 * Nome-Programma is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nome-Programma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.hpp"
#include "ActionManager.hpp"
#include <signal.h>
#include <pthread.h>
#include <fstream>

bool g_quit = false;
pthread_t g_readLoopThread;
pthread_t g_consumerThread;

namespace {
	struct INotifyLimits {
		size_t maxUserInstances;
		size_t maxUserWatches;
		size_t maxQueuedEvents;
	};
	
	///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	void GetINotifyLimits (INotifyLimits* parLimits) {
		const int count = 3;
		const char* const srcPaths[count] = {
			"/proc/sys/fs/inotify/max_user_instances",
			"/proc/sys/fs/inotify/max_user_watches",
			"/proc/sys/fs/inotify/max_queued_events"
		};
		size_t* const destinations[count] = {
			&(parLimits->maxUserInstances),
			&(parLimits->maxUserWatches),
			&(parLimits->maxQueuedEvents)
		};
	
		for (int z = 0; z < count; ++z) {
			std::ifstream src;
			src.open(srcPaths[z], std::ifstream::in);
			src >> *(destinations[z]);
			src.close();
		}
	}
	
	///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	void* readLoop (void* parWatch) {
		wam::ActionManager* actionManager = reinterpret_cast<wam::ActionManager*>(parWatch);
		std::cout << "readLoop() started" << std::endl;
		std::vector<char> buffer_vec(1024 * 4);
		char* buffer = &buffer_vec[0];
	
		while (not g_quit and actionManager->Update(buffer, static_cast<int>(buffer_vec.size())));
		return NULL;
	}
	
	///-----------------------------------------------------------------------------
	///-----------------------------------------------------------------------------
	void shutdown (int parSig) {
		if (SIGINT == parSig) {
			signal(SIGINT, SIG_DFL);
			std::cout << "Shutting down..." << std::endl;
			g_quit = true;
			pthread_cancel(g_readLoopThread);
			//pthread_cancel(g_consumerThread);
		}
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void* ConsumeActions (void* parManager) {
		wam::ActionManager* actionManager = reinterpret_cast<wam::ActionManager*>(parManager);
		while (not g_quit) {
			actionManager->WaitIfEmpty();
			wam::Action currAction;
			std::string currPath;
			std::string currPathTo;
			std::string currFileFrom;
			std::string currFileTo;
			const bool retAction = actionManager->Pop(currAction, currPath, currPathTo, currFileFrom, currFileTo);
			if (retAction) {
				std::cout << "Action retrieved: " << currAction << " " << currPath;
				std::cout << " " << currFileFrom << " --> " << currPathTo << " " << currFileTo << std::endl;
				sleep(5);
			}
		}
	}
} //unnamed namespace

///-----------------------------------------------------------------------------
///-----------------------------------------------------------------------------
int main (const int argc, const char* argv[]) {
	//std::cout << argv[0] << std::endl;
	
	{
		INotifyLimits limits;
		GetINotifyLimits(&limits);
		std::cout << "System inotify settings:\n";
		std::cout << "Max user instances: " << limits.maxUserInstances << "\n";
		std::cout << "Max user watches: " << limits.maxUserWatches << "\n";
		std::cout << "Max queued events: " << limits.maxQueuedEvents << "\n";
	}
	
	wam::ActionManager actionManager;
	actionManager.AddMonitoredDirectory("/home/dev00");
	actionManager.AddMonitoredDirectory("/home/dev00/downloads");

	signal(SIGINT, &shutdown);

	pthread_create(&g_readLoopThread, NULL, &readLoop, &actionManager);
	pthread_create(&g_consumerThread, NULL, &ConsumeActions, &actionManager);
	pthread_join(g_readLoopThread, NULL);
	actionManager.NotifyQuitting();
	pthread_join(g_consumerThread, NULL);

	std::cout << "Quitting program" << std::endl;
	return 0;
}
