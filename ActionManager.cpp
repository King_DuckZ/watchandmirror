/*
 * Copyright King_DuckZ (2012)
 *
 * This file is part of WatchAndMirror.
 *
 * Nome-Programma is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nome-Programma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.hpp"
#include "ActionManager.hpp"
#include "WatchedObject.hpp"
#include "Mutex.hpp"
#include <sys/stat.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <algorithm>

namespace wam {
	namespace {
		const int g_monitoredEvents = IN_ATTRIB | IN_CLOSE_WRITE | IN_MOVED_FROM |
			IN_MOVED_TO | IN_DELETE | IN_CREATE | IN_DELETE_SELF;

		///-------------------------------------------------------------------------
		///-------------------------------------------------------------------------
		size_t GetFileSize (const std::string& parFile) {
			struct stat st;
			stat(parFile.c_str(), &st);
			return st.st_size;
		}

		///---------------------------------------------------------------------
		///---------------------------------------------------------------------
		const char* StringizeAction (Action parAction) {
			switch (parAction) {
			case Action_Delete: return "Action_Delete";
			case Action_Create: return "Action_Create";
			case Action_Edit: return "Action_Edit";
			case Action_Move: return "Action_Move";
			case Action_Attrib: return "Action_Attrib";
			default: return NULL;
			}
		}
	} //unnamed namespace

	struct ActionManager::PackagedData {
		inotify_event* firstEvent;
		inotify_event* secondEvent;
	};

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool FileOpCostIsMore::operator() (const WatchedObject* parA, const WatchedObject* parB) const {
		if (parA == parB)
			return false;
	
		const Action actA = parA->GetAction();
		const Action actB = parB->GetAction();
		{
			const bool actAIsMove = (Action_Move == actA);
			if (actB == Action_Move)
				return not actAIsMove;
			else if (actAIsMove)
				return false;
		}
	
		return (GetFileSize(parA->GetPath() + parA->GetFirstFilename()) > GetFileSize(parB->GetPath() + parB->GetFirstFilename()));
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ActionManager::ActionManager() {
		m_fd = inotify_init();
	}
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	ActionManager::~ActionManager() {
		std::cout << "Closing watches..." << std::endl;
		for (std::map<int, std::string>::iterator itWDPath = m_watchedPaths.begin(), itWDPathEND = m_watchedPaths.end();
			itWDPath != itWDPathEND;
			++itWDPath)
		{

			const int currWD = itWDPath->first;
			inotify_rm_watch(m_fd, currWD);
		}
		close(m_fd);
		m_fd = 0;
	}
	
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void ActionManager::AddFileMoveAction (const PackagedData& parData) {
		const inotify_event* const fromEvent = parData.firstEvent;
		const inotify_event* const toEvent = parData.secondEvent;
		std::cout << "Adding action: " << StringizeAction(Action_Move) << " (" << fromEvent->wd << " --> " << toEvent->wd << "): ";
		std::cout << fromEvent->name << " --> " << toEvent->name << std::endl;
	
		WatchedObject* newObject = new WatchedObject(fromEvent, toEvent, m_watchedPaths);
		{
			ScopedLock lock(m_queueMutex);
			m_queue.push(newObject);
		}
		m_waitIfEmpty.Set();
	}
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void ActionManager::AddAction (Action parAction, const PackagedData& parData) {
		const inotify_event* const event = parData.firstEvent;
		std::cout << "Adding action: " << StringizeAction(parAction) << " (" << event->wd << "): ";
		std::cout << event->name << std::endl;
	
		WatchedObject* newObject = new WatchedObject(event, m_watchedPaths, parAction);
		{
			ScopedLock lock(m_queueMutex);
			m_queue.push(newObject);
		}
		m_waitIfEmpty.Set();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void ActionManager::AddMonitoredDirectory (const char* parPath) {
		const int watch = inotify_add_watch(m_fd, parPath, g_monitoredEvents);
		m_watchedPaths[watch] = std::string(parPath);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool ActionManager::Update (char* parBuffer, int parBufferSize) {
		const int readSize = read(m_fd, parBuffer, parBufferSize);
		if (readSize <= 0) {
			return false;
		}

		inotify_event* event = NULL;
		int treated = 0;
		PackagedData packagedData;
		event = reinterpret_cast<inotify_event*>(parBuffer);
		while (treated < readSize) {
			packagedData.firstEvent = event;
			packagedData.secondEvent = NULL;

			if ((event->mask & IN_MODIFY) && event->len > 0) {
				this->AddAction(wam::Action_Edit, packagedData);
			}
			else if ((event->mask & IN_CREATE) && event->len > 0) {
				this->AddAction(wam::Action_Create, packagedData);
			}
			else if ((event->mask & IN_CLOSE_WRITE) && event->len > 0) {
				this->AddAction(wam::Action_Edit, packagedData);
			}
			else if ((event->mask & IN_DELETE) && event->len > 0) {
				this->AddAction(wam::Action_Delete, packagedData);
			}
			else if (event->mask & IN_DELETE_SELF) {
				this->AddAction(wam::Action_Delete, packagedData);
			}
			else if ((event->mask & IN_ATTRIB) && event->len > 0) {
				this->AddAction(wam::Action_Attrib, packagedData);
			}
			else if ((event->mask & IN_MOVED_FROM) && event->len > 0) {
				std::map<uint32_t, char*>::iterator itOther = m_moves.find(event->cookie);
				if (m_moves.end() != itOther) {
					packagedData.firstEvent = event;
					packagedData.secondEvent = reinterpret_cast<inotify_event*>(itOther->second);
					this->AddFileMoveAction(packagedData);
					delete[] itOther->second;
					m_moves.erase(itOther);
				}
				else {
					const size_t copySize = sizeof(inotify_event) + event->len;
					char* otherBuff = new char[copySize];
					std::copy(parBuffer + treated, parBuffer + treated + copySize, otherBuff);
					m_moves.insert(std::make_pair(event->cookie, otherBuff));
				}
			}
			else if ((event->mask & IN_MOVED_TO) && event->len > 0) {
				std::map<uint32_t, char*>::iterator itOther = m_moves.find(event->cookie);
				if (m_moves.end() != itOther) {
					packagedData.firstEvent = reinterpret_cast<inotify_event*>(itOther->second);
					packagedData.secondEvent = event;
					this->AddFileMoveAction(packagedData);
					delete[] itOther->second;
					m_moves.erase(itOther);
				}
				else {
					const size_t copySize = sizeof(inotify_event) + event->len;
					char* otherBuff = new char[copySize];
					std::copy(parBuffer + treated, parBuffer + treated + copySize, otherBuff);
					m_moves.insert(std::make_pair(event->cookie, otherBuff));
				}
			}
			else {
				std::cout << "Unknown flag" << std::endl;
			}

			const int chunkSize = sizeof(inotify_event) + event->len;
			treated += chunkSize;
			event = reinterpret_cast<inotify_event*>(parBuffer + treated);
		}
		return true;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void ActionManager::WaitIfEmpty() {
		bool isEmpty = false;
		{
			ScopedLock lock(m_queueMutex);
			isEmpty = m_queue.empty();
		}
		if (isEmpty) {
			m_waitIfEmpty.Wait();
			m_waitIfEmpty.Unset();
		}
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool ActionManager::Pop (Action& parAction, std::string& parPath, std::string& parPathSecond, std::string& parFile, std::string& parFileSecond) {
		bool emptyQueue = true;
		WatchedObject* retObj = NULL;

		{
			ScopedLock lock(m_queueMutex);
			if (not m_queue.empty()) {
				retObj = m_queue.top();
				m_queue.pop();
				emptyQueue = false;
			}
		}

		if (emptyQueue) {
			return false;
		}
		else {
			parAction = retObj->GetAction();
			parPath = retObj->GetPath();
			parFile = retObj->GetFirstFilename();
			if (parAction == Action_Move) {
				parPathSecond = retObj->GetSecondPath();
				parFileSecond = retObj->GetSecondFilename();
			}
			delete retObj;
			return true;
		}
	}
} //namespace wam
