/*
 * Copyright King_DuckZ (2012)
 *
 * This file is part of WatchAndMirror.
 *
 * Nome-Programma is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nome-Programma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id772CEDE07D004CF9B663542637E83497
#define id772CEDE07D004CF9B663542637E83497

#include <errno.h>

namespace wam {
	class Mutex {
	public:
		Mutex ( void );
		~Mutex ( void );
	
		void Lock ( void );
		bool TryLock ( void );
		void Unlock ( void );
		pthread_mutex_t& GetInnerMutex ( void ) { return m_mutex; }
	
	private:
		Mutex ( const Mutex& ); //Forbidden
		const Mutex& operator= ( const Mutex& ); //Forbidden

		pthread_mutex_t m_mutex;
	};

	class BlockingCondition {
	public:
		BlockingCondition ( void );
		~BlockingCondition ( void );
	
		void Set ( void );
		void Unset ( void );
		void Set ( bool parState );
		void Wait ( void );

	private:
		BlockingCondition ( const BlockingCondition& ); //Forbidden
		const BlockingCondition& operator= ( const BlockingCondition& ); //Forbidden
		void CreateCondition ( void );

		Mutex m_mutex;
		pthread_cond_t m_condition;
	};

	
	class ScopedLock {
	public:
		explicit ScopedLock ( Mutex& parMutex ) : m_mutex(parMutex) { m_mutex.Lock(); }
		~ScopedLock ( void ) { m_mutex.Unlock(); }
	
	private:
		ScopedLock ( const ScopedLock& ); //Forbidden
		const ScopedLock& operator= ( const ScopedLock& ); //Forbidden
	
		Mutex& m_mutex;
	};

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline Mutex::Mutex() {
		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);
		pthread_mutex_init(&m_mutex, &attr);
		pthread_mutexattr_destroy(&attr);
	}
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline Mutex::~Mutex() {
		pthread_mutex_destroy(&m_mutex);
	}
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline void Mutex::Lock() {
		pthread_mutex_lock(&m_mutex);
	}
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline bool Mutex::TryLock() {
		const int retVal = pthread_mutex_trylock(&m_mutex);
		return static_cast<bool>(EBUSY != retVal);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline void Mutex::Unlock() {
		pthread_mutex_unlock(&m_mutex);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline BlockingCondition::BlockingCondition() {
		CreateCondition();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline BlockingCondition::~BlockingCondition() {
		pthread_cond_destroy(&m_condition);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline void BlockingCondition::Set() {
		pthread_cond_signal(&m_condition);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline void BlockingCondition::Unset() {
		pthread_cond_destroy(&m_condition);
		CreateCondition();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline void BlockingCondition::Set (bool parState) {
		if (parState)
			Set();
		else
			Unset();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline void BlockingCondition::Wait() {
		m_mutex.TryLock();
		pthread_cond_wait(&m_condition, &m_mutex.GetInnerMutex());
		m_mutex.Unlock();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	inline void BlockingCondition::CreateCondition() {
		pthread_condattr_t attr;
		pthread_condattr_init(&attr);
		pthread_cond_init(&m_condition, &attr);
		pthread_condattr_destroy(&attr);
	}
} //namespace wam

#endif
