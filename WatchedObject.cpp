/*
 * Copyright King_DuckZ (2012)
 *
 * This file is part of WatchAndMirror.
 *
 * Nome-Programma is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nome-Programma is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.hpp"
#include "WatchedObject.hpp"

namespace wam {
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	WatchedObject::WatchedObject (const inotify_event* parEvent, const std::map<int, std::string>& parPathDict, Action parAction) :
		m_path(parPathDict.find(parEvent->wd)->second),
		m_path2(),
		m_filename(parEvent->name),
		m_filename2(),
		m_watchID(parEvent->wd),
		m_pendingAction(parAction)
	{
	}
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	WatchedObject::WatchedObject (const inotify_event* parEvent, const inotify_event* parEvent2, const std::map<int, std::string>& parPathDict) :
		m_path(parPathDict.find(parEvent->wd)->second),
		m_path2(parPathDict.find(parEvent2->wd)->second),
		m_filename(parEvent->name),
		m_filename2(parEvent2->name),
		m_watchID(parEvent->wd),
		m_pendingAction(Action_Move)
	{
	}
	
	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	bool WatchedObject::operator< (const WatchedObject& parOther) const {
		const bool pathEql = (this->m_path == parOther.m_path);
		if (pathEql)
			return (m_filename < parOther.m_filename);
		else
			return (this->m_path < parOther.m_path);
	}
} //namespace wam
